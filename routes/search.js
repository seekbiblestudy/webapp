var express = require('express');
var secured = require('../lib/middleware/secured');
var router = express.Router();
var eslasticsearch = require("elasticsearch");


const client = new eslasticsearch.Client({
    host: process.env.ELASTICSEARCH_ENDPOINT,
});


router.get('/search', secured(), function (req, res, next) {

    client.search({
        index: "articles",
        q: req.query.q
    }).then(function(body) {
        console.log("Have results");
        const data = body.hits.hits.map(i =>  { return { url: i._source.url, title: i._source.title, author: i._source.author } });

        res.render('search', {
            results: data,
            title: "Search: " + req.query.q,
          });
    });
});

module.exports = router;
